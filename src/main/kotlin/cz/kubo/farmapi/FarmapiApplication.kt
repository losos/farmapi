package cz.kubo.farmapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FarmapiApplication

fun main(args: Array<String>) {
	runApplication<FarmapiApplication>(*args)
}
